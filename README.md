# Playlister

Shuffle your cmus library by albums and import as playlist.

    # Build
    $ g++ -I. -o playlister playlister.cpp

    # Run, requires a running cmus instance
    $ ./playlister | cmus-remote

# Author, License

(c) 2017, Christoph 'criztovyl' Schulz

GPLv3 and later.
