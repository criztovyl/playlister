#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<map>
#include<list>
#include<algorithm>
#include<ctime>
#include<cstdlib>
#include<cstring>

#include<mystring.cpp>
#include<myexec.cpp>

using namespace std;

class Playlister {
    private:

        static int random(int);

        istringstream *library;
        string format;

        void storeFile(map<string, list<string>>*, vector<string>*, const string, const string);
    public:

        Playlister(istringstream*, string format);

        void makePlaylist();

};

int Playlister::random (int i) { return std::rand()%i; }

void Playlister::storeFile(map<string,list<string>> *albums, vector<string> *albumKeys, const string album, const string file){

    map<string,list<string>>::iterator it = albums->find(album);

    if(it == albums->end()){
        it = albums->insert( it, pair<string,list<string>>(album, list<string>()) );
        albumKeys->push_back(album);
    }

    it->second.push_back(file);
}

void Playlister::makePlaylist(){

    string line, file;
    int counter = 0;
    map<string, list<string>> albums;
    vector<string> albumKeys;
    bool stored = false;
    int unknown_album = 0;

    while(std::getline(*library, line)){
        vector<string> ssplit = split(line, ' ');

        if(ssplit[0] == "file"){

            if(!stored) storeFile(&albums, &albumKeys, "___UNKNOWN ALBUM " + to_string(unknown_album++) + "__", file);

            file = join(vector<string>(ssplit.begin()+1, ssplit.end()), " ");
            stored = false;
        }
        else if(ssplit[0] == "tag" && ssplit[1] == "album"){

            string album = join(vector<string>(ssplit.begin()+2, ssplit.end()), " ");

            if(album == "No Album" || album == "Unbekannt")
                album = "__UNKNOWN_ALBUM " + to_string(unknown_album++) + "__";

            storeFile(&albums, &albumKeys, album, file);

            stored = true;
        }
    }

    random_shuffle(albumKeys.begin(), albumKeys.end(), random);

    int i = 0;

    for(vector<string>::iterator album_it = albumKeys.begin(); album_it != albumKeys.end(); album_it++){
        list<string> files = albums.find(*album_it)->second;
        cerr << *album_it << endl;
        for(list<string>::iterator file_it = files.begin(); file_it != files.end(); file_it++){
            printf((format + "\n").c_str(), file_it->c_str(), i++, album_it->c_str());
        }
    }

}

Playlister::Playlister(istringstream *library, string format){
    this->library = library;
    this->format = format;
}

int main(int argc, char** argv){

    srand( time(0) );

    cerr << "playlister -- shuffle your cmus library" << endl;
    cerr << "(c) 2017 Christoph 'criztovyl' Schulz" << endl;
    cerr << "GPLv3 or later." << endl;


    if(argc != 2){
        cerr << endl << "playlister accepts exactly on argument. use \"help\" for more info." << endl;
        return 1;
    }

    string format = argv[1];

    if(format == "help"){
        cerr << endl << "Usage: format" << endl;
        cerr << "format: A sprintf format or the name of an predefined format." << endl;
        cerr << endl << "Predefined: cmus ubuntu-music-app" << endl;
        return 0;
    }

    string cmds[] = {"view 3", "win-sel-cur", "clear", "set shuffle=false", "set repeat=false", "set play_library=false"};
    string library = exec("cmus-remote -C \"save -l -e -\"");
    istringstream libraryStream (library);

    if(format == "cmus"){

        format = "add -p %s";
        cerr << "sending cmus commands..." << endl;

        for(unsigned int i = 0; i < sizeof(cmds)/sizeof(cmds[0]); i++)
            cout << cmds[i] << endl;
    } else if (format == "ubuntu-music-app"){
        format = "insert into queue values(%2$d, \"%1$s\")";
    }

    cerr << "okay" << endl;

    Playlister playlister = Playlister(&libraryStream, format);
    playlister.makePlaylist();

    cerr << "end." << endl;

    return 0;
}
