// https://stackoverflow.com/questions/236129/split-a-string-in-c#236803
#include <string>
#include <sstream>
#include <vector>
#include <iterator>

template<typename Out>
extern void split(const std::string &s, char delim, Out result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}


extern std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

// https://stackoverflow.com/questions/1430757/c-vector-to-string#1430774
extern std::string join(std::vector<std::string> v, const std::string sep){
    std::stringstream ss;
    for(size_t i = 0; i < v.size(); ++i)
    {
        if(i != 0)
            ss << sep;
        ss << v[i];
    }
    return ss.str();
}
